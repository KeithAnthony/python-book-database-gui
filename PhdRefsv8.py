import glob
import hashlib
import math
import os
import pandas as pd
from pathlib import Path
import subprocess
import sys
import re
import io
import datetime, time

# emailing files

#import yagmail

# PyQt items

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

# Toggles 

TITLESELECTED = False
DEBUG = False
VERBOSE = False
CHECKCONSISTENCY = True
CHECKFORNEW = True

# Callbacks

class myClass (QWidget):
    def __init__(self):
        super().__init__()
        title = 'my PhD File Manager'
        self.setWindowTitle(title)
        self.initUI()

    def getPathList(self):
        '''
        Modified: 26 Feb 2023
        -------
        Returns
        -------
        files_paths : list o ffile paths
            of all files of the types listed in subdirectories
        '''
        print(os.getcwd())
        dir_to_exclude = []
        #files = glob.glob('**/*.{pdf,zip,mp3,mp4}', recursive=True)
        files = glob.glob('**/*.pdf', recursive=True) + \
                glob.glob('**/*.zip', recursive=True) + \
                glob.glob('**/*.mp4', recursive=True) + \
                glob.glob('**/*.mp3', recursive=True) + \
                glob.glob('**/*.vob', recursive=True)
        files_paths = [_ for _ in files if _.split("\\")[0] not in dir_to_exclude]
        return files_paths

    def calculateMD5(self,filename):
        h = hashlib.sha1()
        with open(filename,'rb') as file:
            chunk = 0
            while chunk != b'':
                # read only 1024 bytes at a time
                chunk = file.read(32768)
                h.update(chunk)
        return h.hexdigest()
    
    def generateLists(self, DF):
        pathlist = DF['path'].to_list()
        titlelist = DF['title'].to_list()
        hashlist = DF['hashvalues'].to_list()
        publist = sorted(DF['pub'].astype(str).str.strip().unique())
        yoplist = sorted(DF['yop'].astype(str).str.strip().unique())
        #taglist = sorted(DF['tags'].astype(str).str.split(',').explode().str.strip().unique())
        
        authorlist = []
        authors = DF['authors'].to_list()
        for item in authors:
            badlist = ['ed','ed.','eds','eds.','et al','et al.','etal','etal eds','etal eds.','']
            for item2 in item.split(","):
                author = item2.strip()
                if (author not in authorlist) and (author not in badlist):
                    authorlist.append(author)
        authorlist = [item.strip() for item in authorlist]
        authorlist.sort()
        
        bigtaglist = []
        tags = DF['tags'].to_list()
        for item in tags:
            for item2 in str(item).split(","):
                tag = item2.strip()
                if (tag not in bigtaglist):
                    bigtaglist.append(tag)
        taglist = [item.strip() for item in bigtaglist]
        taglist.sort()
        
        # return results
        return (titlelist,pathlist, hashlist, publist, yoplist, taglist, authorlist)
        
    def getfileAttributes(self,name):
        '''
        file is a relative file path, with filename in an expected
        format
        '''
        print(name)
        filen = re.split(r'\\', name, 1)[1]
        yop, rest = filen.strip().split(" ",1) 
        pub = re.split(r'[(]',rest)[0]
        rest = re.split(r'[(]',rest)[1]
        authors  = re.split(r'[)]',rest)[0]
        title = re.split(r'[)]',rest)[1]
        MD5hash = self.calculateMD5(name)
        tag = ''
        bibtex = ' '
        note = ' '
        return (filen,yop,pub,authors,title,MD5hash,tag,bibtex,note)

    def checkConsistency(self,df):
        count = 0
        for i, row in df.iterrows():
            file_path = row['path']
            if not os.path.exists(file_path):
                count += 1
                print(count)
                print(f"File does not exist: {file_path}")
                df.drop(i, inplace=True)
        return df
    
    def checkforNew(self):
        print("Inside checkfornew")
        
    #def checkforNew(self,df):
    #    pass

    def initUI(self):
        #self.db - []
        grid = QGridLayout()
        self.setLayout(grid)
        #
        # Check for datafile existence
        #
        if os.path.exists("pdfDB.csv"):
            pdfDB = pd.read_csv('pdfDB.csv', sep='\t')
            # Check that path is valid
            #
            if CHECKCONSISTENCY:
                pdfDB = self.checkConsistency(pdfDB)
                
            if CHECKFORNEW:
                self.checkforNew()
            
            pdfDB.to_csv('pdfDB.csv', sep='\t', index=False)
            pdfDB = pd.read_csv('pdfDB.csv', sep='\t')
            print(len(pdfDB),pdfDB)
            
            #checkForFilePointedToByDataFrame(pdfDB)
            ''' 
            Check for new files
            '''
            
            hashlist = pdfDB['hashvalues'].to_list()
            if VERBOSE:
                print(hashlist)
            DATETIMESTAMP = os.path.getmtime("pdfDB.pkl")
            converted = datetime.datetime.fromtimestamp(DATETIMESTAMP)
            print("Database file exists, with mod time of",converted)
            # Check for new files:
            # Will have a more recent timestand AND a unique MD5hash
            count = 0
            for name in self.getPathList():
                #if (self.calculateMD5(name) not in hashlist):
                if (self.calculateMD5(name) not in hashlist):
                    count +=1
                    print(count,"New file:",name)
                    if VERBOSE:
                        print(name)
                    filen,yop,pub,authors,title,MD5hash,tag,bibtex,note = self.getfileAttributes(name)
                    newRow = pd.DataFrame(columns=['path','yop','pub','authors','title','hashvalues','tags','bibtexs','notes'])
                    newRow.at[0,'path'] = name
                    newRow.at[0,'yop'] = yop
                    newRow.at[0,'pub'] = pub
                    newRow.at[0,'authors'] = authors
                    newRow.at[0,'title'] = title
                    newRow.at[0,'hashvalues'] = MD5hash
                    newRow.at[0,'tags'] = tag
                    newRow.at[0,'bibtexs'] = bibtex
                    newRow.at[0,'notes'] = note
                    
                    pdfDB = pd.concat([pdfDB, newRow])
                    #SAVE
                    pdfDB.to_csv('pdfDB.csv', sep='\t', index=False)
                    pdfDB = pdfDB.sort_values(by=['path'])
                    pdfDB = pd.read_csv('pdfDB.csv', sep='\t')
            # At this point database shoudl be current!
            self.titlelist,self.pathlist,hashlist,self.publist,self.yopslist,self.taglist,self.authors = self.generateLists(pdfDB)
            
        # else reparse the file structure
        else:
            print("database file pdfDB.csv does NOT exist")
            # TBW
            pass
        
        # callbacks
        def resetBookListWidget():
            print("resetting book list")
            populateBookList()
            
        def populateBookList():
            self.booklist.clear()
            for item in sorted(self.pathlist):
                self.booklist.addItem(item)

        def populateAuthorList():
            self.authorList.clear()
            for item in sorted(self.authors):
                self.authorList.addItem(item)     
 
        def populatePublisherList():
            self.publisherList.clear()
            for item in self.publist:
                self.publisherList.addItem(item)     

        def populateYOP():
            self.yopList.clear()
            for item in sorted(self.yopslist):
                self.yopList.addItem(str(item) )   
                
        def populateTAG():
            self.TAGList.clear()
            for item in self.taglist:
                self.TAGList.addItem(item)

        def titleSelect():
            print("Test Text:",self.selectedbookTitle.text())
            self.selectTitle = self.booklist.currentItem().text()
            self.dbRowNumber = pdfDB[pdfDB['path'] == self.selectTitle].index[0]
            print("Selected df row:",self.dbRowNumber)
            
            self.selectedbookTitle.setText(pdfDB.loc[self.dbRowNumber, 'path'])
            self.selectedbookAuthor.setText(pdfDB.loc[self.dbRowNumber, 'authors'])
            self.selectedbookMD5Hash.setText(pdfDB.loc[self.dbRowNumber, 'hashvalues'])
            self.selectedbookTAGS.setText(str(pdfDB.loc[self.dbRowNumber, 'tags']))
            self.selectedbookPublisher.setText(pdfDB.loc[self.dbRowNumber, 'pub'])
            self.selectedbookBibtex.setText(pdfDB.loc[self.dbRowNumber, 'bibtexs'])
            self.selectedbookNOTES.setText(pdfDB.loc[self.dbRowNumber, 'notes'])
            
            print("Test Text:",self.selectedbookTitle.text())
             
        def authorSelect():
            self.selectAuthor = self.authorList.currentItem().text()
            print("Author chosen:",self.selectAuthor)
            
        def publisherSelect():
            self.selectedPublisher = self.publisherList.currentItem().text()
            print("Publisher chosen:",self.selectedPublisher)
            
        def yopSelect():
            self.selectedYOP = self.yopList.currentItem().text()
            print("YOP chosen:",self.selectedYOP)
            
        def TAGSelect():
            self.selectedTAG = self.TAGList.currentItem().text()
            print("TAG chosen:",self.selectedTAG)
            
        
        def openPDF():
            print("Button to open file, pressed")
            print(self.selectTitle)
    
            os.startfile(self.selectTitle)
            
        def searchForAuthors(): 
            self.booklist.clear()
            searchDB = pdfDB[pdfDB['authors'].astype(str).str.contains(self.selectAuthor)]
            print(searchDB)
            pathlist = searchDB['path'].to_list()
            for item in pathlist:
                self.booklist.addItem(item)

        def searchInNotes(): 
            print("Inside seacrhInNotes function")
            
            query = self.titleSearchTerm.text()
            if not query:
                return
            query_list = [term.strip() for term in query.split(",")]
            print(query_list)
            temp = pdfDB
            for term in query_list:
                temp = temp[temp['notes'].str.contains(term)]
            print(temp)
            self.booklist.clear()
            my_list = temp['path'].tolist()
            for item in my_list:
                self.booklist.addItem(item)
            


        def searchForPubisher():
            self.booklist.clear()
            searchDB = pdfDB[pdfDB['pub'].astype(str).str.contains(self.selectedPublisher)]
            print(searchDB)
            pathlist = searchDB['path'].to_list()
            for item in pathlist:
                self.booklist.addItem(item)
    
        def searchForYOP():
            self.booklist.clear()
            searchDB = pdfDB[pdfDB['yop'].astype(str).str.contains(self.selectedYOP)]
            print(searchDB)
            pathlist = searchDB['path'].to_list()
            for item in pathlist:
                self.booklist.addItem(item)

        def searchForTAG():
            self.booklist.clear()
            searchDB = pdfDB[pdfDB['tags'].astype(str).str.contains(self.selectedTAG)]
            print(searchDB)
            pathlist = searchDB['path'].to_list()
            for item in pathlist:
                self.booklist.addItem(item)
                
        def saveOneBookData():
            pdfDB.loc[self.dbRowNumber, 'bibtexs'] = self.selectedbookBibtex.toPlainText()
            pdfDB.loc[self.dbRowNumber, 'notes'] = self.selectedbookNOTES.toPlainText()
            pdfDB.loc[self.dbRowNumber, 'tags'] = self.selectedbookTAGS.toPlainText()
            pdfDB.to_csv('pdfDB.csv', sep='\t', index=False)
            print(self.selectedbookTAGS.toPlainText())
            # Test code below
            (self.titlelist, pathlist, hashlist, publist, yoplist, self.taglist, authorlist) = self.generateLists(pdfDB)
            self.TAGList.clear()
            for item in self.taglist:
                self.TAGList.addItem(item)
                
        def updateBookData(self):
            print("Inside updateBookData")
            newTag = self.selectedbookTAGS.toPlainText()
            print("TAGS",newTag)
            print("Notes",self.selectedbookNOTES.toPlainText())
            print("Bibtex",self.selectedbookBibtex.toPlainText())
            
            print("Starting text",pdfDB.loc[self.dbRowNumber, 'tags'])
            #pdfDB.loc[self.dbRowNumber, 'bibtexs'] = self.selectedbookBibtex.toPlainText()
            #pdfDB.loc[self.dbRowNumber, 'notes'] = self.selectedbookNOTES.toPlainText()
            pdfDB.loc[self.dbRowNumber, 'tags'] = newTag
            print("Updated text",pdfDB.loc[self.dbRowNumber, 'tags'])
             
            '''
            self.selectedbookTitle.setText(pdfDB.loc[self.dbRowNumber, 'path'])
            self.selectedbookAuthor.setText(pdfDB.loc[self.dbRowNumber, 'authors'])
            self.selectedbookMD5Hash.setText(pdfDB.loc[self.dbRowNumber, 'hashvalues'])
            self.selectedbookTAGS.setText(str(pdfDB.loc[self.dbRowNumber, 'tags']))
            self.selectedbookPublisher.setText(pdfDB.loc[self.dbRowNumber, 'pub'])
            self.selectedbookBibtex.setText(pdfDB.loc[self.dbRowNumber, 'bibtexs'])
            self.selectedbookNOTES.setText(pdfDB.loc[self.dbRowNumber, 'notes'])
            '''

        def searchInTitles():
            query = self.titleSearchTerm.text()
            if not query:
                return
            query_list = [term.strip() for term in query.split(",")]
            print(query_list)
            temp = pdfDB
            for term in query_list:
                temp = temp[temp['title'].str.contains(term)]
            print(temp)
            self.booklist.clear()
            my_list = temp['path'].tolist()
            for item in my_list:
                self.booklist.addItem(item)

        bookTotal = QLineEdit()
        grid.addWidget(bookTotal, 0, 0, 1, 1)
        bookTotal.setText(str(len(pdfDB)))

        ##
        ## BOOK lIST
        ##
        
        self.booklistLabel = QLabel('Titles')
        grid.addWidget(self.booklistLabel, 0, 1, 1 , 2)
        
        self.resetBookList = QPushButton('Reset Book List')
        grid.addWidget(self.resetBookList, 1, 0, 1, 1)        
        self.resetBookList.setStyleSheet("background-color : yellow; color: black")
        self.resetBookList.clicked.connect(resetBookListWidget)
        
        self.booklist = QListWidget()
        grid.addWidget(self.booklist, 2, 0, 4, 7)
        self.booklist.clicked.connect(titleSelect)
        
        self.openFile = QPushButton('Open File')
        grid.addWidget(self.openFile, 1, 1, 1, 1)
        self.openFile.setStyleSheet("background-color : blue; color: white")
        self.openFile.clicked.connect(openPDF)
        
        ##
        ## AUTHORS
        ##
        
        self.authorlistLabel = QLabel('Authors')
        grid.addWidget(self.authorlistLabel, 0, 7, 1 , 1)  
        
        self.authorList = QListWidget()
        grid.addWidget(self.authorList, 2, 7, 4, 3)
        populateAuthorList()
        self.authorList.clicked.connect(authorSelect)
        
        self.authorSearch = QPushButton('Search for books by authors')
        grid.addWidget(self.authorSearch, 1, 7, 1, 1)
        self.authorSearch.setStyleSheet("background-color : green; color: white")
        self.authorSearch.clicked.connect(searchForAuthors)      
        
        #
        # Publishers
        #

        self.publisherlistLabel = QLabel('Publishers')
        grid.addWidget(self.publisherlistLabel, 0, 11, 1 , 2)   

        self.publisherSearch = QPushButton('Search for books by publishers')
        grid.addWidget(self.publisherSearch, 1, 11, 1, 1)
        self.publisherSearch.setStyleSheet("background-color : green; color: white")
        self.publisherSearch.clicked.connect(searchForPubisher)

        self.publisherList = QListWidget()
        grid.addWidget(self.publisherList, 2, 11, 4, 1)
        populatePublisherList()
        self.publisherList.clicked.connect(publisherSelect)

        # 
        # yop
        #
        
        self.yoplistLabel = QLabel('Year of Publication')
        grid.addWidget(self.yoplistLabel, 0, 12, 1 , 2)   

        self.yopSearch = QPushButton('Search by YOP')
        grid.addWidget(self.yopSearch, 1, 12, 1, 1)
        self.yopSearch.setStyleSheet("background-color : green; color: white")
        self.yopSearch.clicked.connect(searchForYOP)

        self.yopList = QListWidget()
        grid.addWidget(self.yopList, 2, 12, 4, 1)
        populateYOP()
        self.yopList.clicked.connect(yopSelect)

        #
        # TAGS
        #
        
        self.taglistLabel = QLabel('TAGS')
        grid.addWidget(self.taglistLabel, 0, 13, 1 , 3)   

        self.TAGSearch = QPushButton('Search by TAG')
        grid.addWidget(self.TAGSearch, 1, 13, 1, 1)
        self.TAGSearch.setStyleSheet("background-color : green; color: white")
        self.TAGSearch.clicked.connect(searchForTAG)

        self.TAGList = QListWidget()
        grid.addWidget(self.TAGList, 2, 13, 4, 1)
        populateTAG()
        self.TAGList.clicked.connect(TAGSelect)
   
        ##############################
        ############################## SEPARATOR
        ##############################
        
        self.Hseparator = QLabel()
        self.Hseparator.setFrameStyle(QFrame.HLine | QFrame.Raised)

        self.Hseparator.setLineWidth(4)
        grid.addWidget(self.Hseparator, 6, 0, 1, 15)
        
        
        self.authorSearch = QPushButton('Search in Titles')
        grid.addWidget(self.authorSearch, 7, 0, 1, 1)
        self.authorSearch.setStyleSheet("background-color : green; color: white")
        self.authorSearch.clicked.connect(searchInTitles)  
        
        self.titleSearchTerm = QLineEdit()
        grid.addWidget(self.titleSearchTerm, 7, 1, 1, 8)
        
        self.notesSearch = QPushButton('Search in Notes')
        grid.addWidget(self.notesSearch, 7, 10, 1, 2)
        self.notesSearch.setStyleSheet("background-color : green; color: white")
        self.notesSearch.clicked.connect(searchInNotes)  
        
        self.Hseparator = QLabel()
        self.Hseparator.setFrameStyle(QFrame.HLine | QFrame.Raised)

        self.Hseparator.setLineWidth(4)
        grid.addWidget(self.Hseparator, 8, 0, 1, 15)
        
        ##############################
        ############################## SELECTED BOOK
        ##############################
        
        self.selectedBookTitleLabel = QLabel('Title')
        #self.selectedBookTitleLabel.setAlignment(Qt.AlignRight)
        grid.addWidget(self.selectedBookTitleLabel, 9, 0, 1, 1)
        self.selectedbookTitle = QLineEdit()
        grid.addWidget(self.selectedbookTitle, 9, 1, 1, 10)
        
        self.selectedBookHashLabel = QLabel('MD5 Hash')
        grid.addWidget(self.selectedBookHashLabel, 10, 0, 1, 1)
        self.selectedbookMD5Hash = QLineEdit()
        grid.addWidget(self.selectedbookMD5Hash, 10, 1, 1, 10)
        
        self.selectedBookAuthorLabel = QLabel('Author')
        grid.addWidget(self.selectedBookAuthorLabel, 11, 0, 1, 1)
        self.selectedbookAuthor = QLineEdit()
        grid.addWidget(self.selectedbookAuthor, 11, 1, 1, 10)
        
        self.selectedBookPublisherLabel = QLabel('Publisher')
        grid.addWidget(self.selectedBookPublisherLabel, 12, 0, 1, 1)
        self.selectedbookPublisher = QLineEdit()
        grid.addWidget(self.selectedbookPublisher, 12, 1, 1, 10)
        
        self.selectedBookBibtexLabel = QLabel('Bibtex')
        grid.addWidget(self.selectedBookBibtexLabel, 14, 0, 1, 1)
        self.selectedbookBibtex = QTextEdit()
        grid.addWidget(self.selectedbookBibtex, 14, 1, 4, 7)
        
        self.selectedBookTAGSLabel = QLabel('TAGS')
        self.selectedBookTAGSLabel.setAlignment(Qt.AlignRight)
        grid.addWidget(self.selectedBookTAGSLabel, 14, 11, 1, 1)
        self.selectedbookTAGS = QTextEdit()
        grid.addWidget(self.selectedbookTAGS, 14, 12, 4, 5)
        
        self.selectedBookNOTESLabel = QLabel('NOTES')
        self.selectedBookNOTESLabel.setAlignment(Qt.AlignRight)
        grid.addWidget(self.selectedBookNOTESLabel, 9, 11, 1, 1)
        self.selectedbookNOTES = QTextEdit()
        grid.addWidget(self.selectedbookNOTES, 9, 12, 4, 5)
        
        
        self.resetBookList = QPushButton('Update Book Data')
        grid.addWidget(self.resetBookList, 13, 0, 1, 1)        
        self.resetBookList.setStyleSheet("background-color : red; color: black")
        self.resetBookList.clicked.connect(saveOneBookData)

        #### Show GUI
        self.show()
        

if __name__ == '__main__':
    app = QApplication(sys.argv)
    thisone = myClass()
    sys.exit(app.exec_())
